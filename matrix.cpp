#include "matrix.h"


Matrix::Matrix(const short int nLines, const short int nColumns)
:nLines(nLines),nColumns(nColumns){
	
    elements.resize(nColumns);
    for (int i = 0; i < nColumns; ++i)
        elements[i].resize(nLines);
		
    for(int i = 0; i < nColumns; i++)
        for(int j = 0; j < nLines; j++)
            elements[i][j] = 0;
}

void Matrix::resize(const short int nLines, const short int nColumns){
	
	this->nLines = nLines;
	this->nColumns = nColumns;
	
    elements.resize(nColumns);
    for (int i = 0; i < nColumns; ++i)
        elements[i].resize(nLines);

    for(int i = 0; i < nColumns; i++)
        for(int j = 0; j < nLines; j++)
            elements[i][j] = 0;
}

short int Matrix::getnLines(){
	return nLines;
}

short int Matrix::getnColumns(){
	return nColumns;
}

float Matrix::get(const short int row, const short int col){
    if(col >= 0 && col < nColumns && row >= 0 && row < nLines)
        return elements[col][row];
    else
        std::cout << "Assignment matrix dimentions mismatch! () operand" << std::endl;
    return -1;
}

Matrix& Matrix::operator=( const Matrix& other ){
	if(this->nLines == other.nLines && this->nColumns == other.nColumns){
		for(int i = 0; i < nColumns; i++)
			for(int j = 0; j < nLines; j++)
				elements[i][j] = other.elements[i][j];
		return *this;
	}else{
        std::cout << "Assignment matrix dimentions mismatch! = operand" << std::endl;
		return *this;
	}
}

Matrix Matrix::operator+( const Matrix& other ) const{
    Matrix aux(other.nLines,other.nColumns);
    if(this->nLines == other.nLines && this->nColumns == other.nColumns)
		for(int i = 0; i < nColumns; i++)
			for(int j = 0; j < nLines; j++)
                aux(i,j) = this->elements[i][j] + other.elements[i][j];
    else
        std::cout << "Assignment matrix dimentions mismatch! + operand" << std::endl;
    return aux;
}

Matrix Matrix::operator-( const Matrix& other ) const {
    Matrix aux(other.nLines,other.nColumns);
    if(this->nLines == other.nLines && this->nColumns == other.nColumns)
        for(int i = 0; i < nColumns; i++)
            for(int j = 0; j < nLines; j++)
                aux(i,j) = this->elements[i][j] - other.elements[i][j];
    else
        std::cout << "Assignment matrix dimentions mismatch! - operand" << std::endl;
    return aux;
}

Matrix Matrix::operator*( const Matrix& other ) const{
    Matrix aux(this->nLines,other.nColumns);
    if(this->nColumns == other.nLines){
        for(int i = 0; i < this->nLines; i++){
            for(int j = 0; j < other.nColumns; j++){
                float temp = 0;
                for(int m = 0; m < this->nColumns; m++)
                    temp += this->elements[m][i]*other.elements[j][m];
                aux(i,j) = temp;
			}
        }
	}else{
        std::cout << "Assignment matrix dimentions mismatch! * operand" << std::endl;
	}
    return aux;
}

float& Matrix::operator() (const short int row, const short int col){
    if(col >= 0 && col < nColumns && row >= 0 && row < nLines)
        return elements[col][row];
    else
        std::cout << "Assignment matrix dimentions mismatch! () operand" << std::endl;
    return elements[0][0];
}

float Matrix:: operator() (const short int row, const short int col) const{
    if(col >= 0 && col < nColumns && row >= 0 && row < nLines)
        return elements[col][row];
    else
        std::cout << "Assignment matrix dimentions mismatch! () operand" << std::endl;
    return -1;
}

std::ostream& operator<<( std::ostream &os, Matrix& other ){
    for(int i = 0; i < other.getnLines(); i++){
		os << "\n";
        for(int j = 0; j < other.getnColumns(); j++)
            os << other.get(i,j) << " ";
	}		
	return os;
}
