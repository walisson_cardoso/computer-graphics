
#include <bitset>
#include <cstdlib>
#include <cmath>
#include <algorithm>

#include "polygon.h"
#include "buffer.h"
#include "matrix.h"

#define PI 3.14159265

enum colors {BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE};
enum tools {PENCIL, LINE, POLYLINE, POLYGON, RECTANGLE, FILL, CUT};
enum line_cut_status {LINE_IN, LINE_OUT, LINE_CUT, INVALID_LINE};

class Tech {
public:
    Tech();
    Tech(const short int nLines, const short int nColumns);
    void setBufferDimentions(const short int nLines, const short int nColumns);
    short int getPixel(const short int x, const short int y);
    void drawPixel (const short int x, const short int y, const short int color);
    void bresenham (const short int x1, const short int y1, const short int x2, const short int y2, const short int color);
	void drawLine  (const short int x1, const short int y1, const short int x2, short const int y2, const short int color);
	void drawHeart (const short int x, const short int y, const short int color);
	void drawCircle(const short int x, const short int y, const short int r, const short int color);
	void drawRectangle(const short int botton, const short int up, const short int left, short const int right, const short int color);
    void scanLine(Polygon &p);
	void addPolygon(Polygon &poly);
    void addPolygonAt(const int index, Polygon &poly);
	void addPoint(const int index, const short int x, const short int y);
	void updateBuffer();
    void clear();
	void setPolygon(const int index, const Polygon &poly);
    void setPosition(const int polyId, const int pointId, const short int x, const short int y);
    void setPosition(const int polyId, const int pointId, const short int x, const short int y, const short int z);
    void setVisibility(const short int polyId, const short int pointId, const bool visibility);
    void cutRegion(const short int x1, const short int y1, const short int x2, short const int y2);
    void cohenSutherland(const short int botton, const short int up, const short int left, short const int right);
    void sutherlandHodgman(const short int botton, const short int up, const short int left, short const int right);
    void translate(const int dx, const int dy, const int dz, const bool onlyCut);
    void rotate(const int degree, const bool onlyCut);
    void scale(const float dx, const float dy, const float dz, const bool onlyCut);
    int  splitLine(Polygon poly, short int &diffBit, short int &x_i, short int &y_i, const short int botton, const short int up, const short int left, short const int right);
    Polygon getPolygon(const short int index);
    int getCutIndex(const int index);
    int getNCutPoly();
	int getNPolygons();
private:
    std::vector< Polygon > polygons;
    std::vector< int > cutIndexes;
    Buffer buffer;
};
