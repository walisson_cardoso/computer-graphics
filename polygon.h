
#include "point.h"

enum type {T_POINT, T_LINE, T_POLYLINE, T_POLYGON, T_CIRCLE};

class Polygon {
public:
	Polygon(const short int type);
	Polygon(const short int type, const short int color);
	Polygon(const short int type, const short int color, const bool filled);
	Polygon(const Polygon &other);
	void addPoint(const Point &p);
	void addPoint(const short int x, const short int y);
	void addPoint(const short int x, const short int y, const short z);
	void setPos(const short int index, const short int x, const short int y, const short int z);
    void setVisibility(const short int index, const bool visibility);
    void clear();
    short int getX(const short int index);
	short int getY(const short int index);
	short int getZ(const short int index);
	short int getColor();
	short int getNPoints();
	short int getType();
	Point getPoint(const short int index);
	bool isFilled();
	Polygon& operator=( const Polygon& other );
private:
	short int type;
	short int color;
	bool filled;
	std::vector<Point> points;
};
