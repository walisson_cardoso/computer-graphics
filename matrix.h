#include <iostream>
#include <vector>

class Matrix {
public:
    Matrix(const short int nLines, const short int nColumns);
	void resize(const short int nLines, const short int nColumns);
	short int getnLines();
	short int getnColumns();
    float get(const short int x, const short int y);
    Matrix& operator=( const Matrix& other );
    Matrix operator+( const Matrix& other ) const;
    Matrix operator-( const Matrix& other ) const;
    Matrix operator*( const Matrix& other ) const;
    float& operator() (const short int row, const short int col);
    float  operator() (const short int row, const short int col) const;
    friend std::ostream& operator<<( std::ostream &os, Matrix& other );
private:
	short int nLines;
	short int nColumns;
    std::vector< std::vector<float> > elements;
};
