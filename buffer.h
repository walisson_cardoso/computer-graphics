
#include <iostream>
#include <vector>

class Buffer {
public:
	Buffer();
	Buffer(const short int nLines, const short int nColumns);
	void resize(const short int nLines, const short int nColumns);
	void set(const short int x, const short int y, const short int color);
	void clear();
	short int get(const short int x, const short int y);
	short int getnLines();
	short int getnColumns();
private:
	short int nLines;
	short int nColumns;
	std::vector< std::vector<short int> > buffer;
};

