
#include "buffer.h"

Buffer::Buffer(){
	
}

Buffer::Buffer(const short int nLines, const short int nColumns){
	this->nLines = nLines;
	this->nColumns = nColumns;
	
	buffer.resize(nColumns);
	for (int i = 0; i < nColumns; ++i)
		buffer[i].resize(nLines);
		
	for(int i = 0; i < nColumns; i++)
		for(int j = 0; j < nLines; j++)
			buffer[i][j] = -1;
}

void Buffer::resize(const short int nLines, const short int nColumns){
	this->nLines = nLines;
	this->nColumns = nColumns;
	
	buffer.resize(nColumns);
	for(int i = 0; i < nColumns; ++i)
		buffer[i].resize(nLines);
		
	for(int i = 0; i < nColumns; i++)
		for(int j = 0; j < nLines; j++)
			buffer[i][j] = -1;
}

void Buffer::set(const short int x, const short int y, const short int color){
	if(x >= 0 && x < nColumns && y >= 0 && y < nLines){
        buffer[x][y] = color;
    }
}

void Buffer::clear(){
	for(int i = 0; i < nColumns; i++)
		for(int j = 0; j < nLines; j++)
			buffer[i][j] = -1;
}

short int Buffer::get(const short int x, const short int y){
	if(x < 0 || x >= nColumns || y < 0 || y >= nLines){
        return -1;
    }else{
		return buffer[x][y];
	}
}

short int Buffer::getnLines(){
	return nLines;
}

short int Buffer::getnColumns(){
	return nColumns;
}
