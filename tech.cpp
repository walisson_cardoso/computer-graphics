#include "tech.h"


Tech::Tech(){
	
}

Tech::Tech(const short int nLines, const short int nColumns)
:buffer(nLines,nColumns){

}

void Tech::setBufferDimentions(const short int nLines, const short int nColumns){
	buffer.resize(nLines, nColumns);
}

short int Tech::getPixel(const short int x, const short int y){
    if(x < 0 || x >= buffer.getnColumns() || y < 0 || y >= buffer.getnLines())
        return -1;
    else
        return buffer.get(x,y);
}

void Tech::drawPixel(const short int x, const short int y, const short int color){
	if(x >= 0 && x < buffer.getnColumns() && y >= 0 && y < buffer.getnLines()){
        buffer.set(x,y,color);
    }
}

void Tech::bresenham (const short int o_x1, const short int o_y1, const short int o_x2, const short int o_y2, const short int color){
	short int x1, x2, y1, y2;
	
	if(o_x1 < o_x2){
		x1 = o_x1; x2 = o_x2; y1 = o_y1; y2 = o_y2;
	}else{
		x2 = o_x1; x1 = o_x2; y2 = o_y1; y1 = o_y2;
	}
	
    drawPixel(x1,y1,color);
	
    int deltaX = abs(x1 - x2);
    int deltaY = abs(y1 - y2);
    float m = float(deltaY) / deltaX;

    bool reflete = false, trocaY = false;

    if(y1 > y2){
        trocaY  = true;
        short int auY = y1; y1 = y2; y2 = auY;
        m = float(abs(y1 - y2)) / abs(x1 - x2);
    }
    
    if(m > 1){
        reflete = true;
        short int auX = x1; x1 = y1; y1 = auX;
            auX = x2; x2 = y2; y2 = auX;
        m = float(abs(y1 - y2)) / abs(x1 - x2);
    }

    float e = m - 0.5;
    short int x = x1, y = y1;

    for(int i = x; i < x2; i++){
        if(e >= 0){
            y = y + 1;
            e = e - 1;
        }
        x = x + 1;
        e = e + m;

        short int x_f = x, y_f = y;

        if(reflete){
            short int auX = x_f; x_f = y_f; y_f = auX;
        }
        
        if(trocaY){
            y_f = o_y1 - y_f + o_y2;
        }

        drawPixel(x_f,y_f,color);
    }
}

void Tech::drawLine(const short int x1, const short int y1, const short int x2, short const int y2, const short int color){
    bresenham(x1, y1, x2, y2, color);
}

void Tech::drawHeart(const short int x, const short int y, const short int color){
    drawPixel(x+1, y,   color);
    drawPixel(x+2, y,   color);
    drawPixel(x+4, y,   color);
    drawPixel(x+5, y,   color);
    drawPixel(x  , y+1, color);
    drawPixel(x+3, y+1, color);
    drawPixel(x+6, y+1, color);
    drawPixel(x  , y+2, color);
    drawPixel(x+6, y+2, color);
    drawPixel(x+1, y+3, color);
    drawPixel(x+5, y+3, color);
    drawPixel(x+2, y+4, color);
    drawPixel(x+4, y+4, color);
    drawPixel(x+3, y+5, color);
}

void Tech::drawCircle(const short int x_center, const short int y_center, const short int radius, const short int color){
    int x = 0;
    int y = radius;
    int p = 1-radius;

    drawPixel(x_center+x,y_center+y,color);
    drawPixel(x_center+x,y_center-y,color);
    drawPixel(x_center-x,y_center+y,color);
    drawPixel(x_center-x,y_center-y,color);
    drawPixel(x_center+y,y_center+x,color);
    drawPixel(x_center+y,y_center-x,color);
    drawPixel(x_center-y,y_center+x,color);
    drawPixel(x_center-y,y_center-x,color);
    while(x < y){
        x++;
        if(p < 0){
            p += 2*x + 3;
        }else{
            y--;
            p += 2*x - 2*y + 5;
        }
        drawPixel(x_center+x,y_center-y,color);
        drawPixel(x_center+x,y_center+y,color);
        drawPixel(x_center-x,y_center-y,color);
        drawPixel(x_center-x,y_center+y,color);
        drawPixel(x_center+y,y_center-x,color);
        drawPixel(x_center+y,y_center+x,color);
        drawPixel(x_center-y,y_center-x,color);
        drawPixel(x_center-y,y_center+x,color);
    }
}

void Tech::drawRectangle(const short int botton, const short int up, const short int left, short const int right, const short int color){
	if(up < botton || right < left){
		std::cout << "\nInvalid Rectangle!!" << std::endl;
	}
	
	for(short int i = left; i <= right; i++) {
		drawPixel(i,up,color);
		drawPixel(i,botton,color);
	}
	for(short int i = botton; i <= up; i++) {
		drawPixel(left,i,color);
		drawPixel(right,i,color);
	}
}

void Tech::scanLine(Polygon &p){
    short int color = p.getColor();
    int size = p.getNPoints()-1;
    if(size < 3)
        return;
    Matrix sides(size,4);

    for(int i = 0; i < size; i++){
        sides(i,0) = std::min(p.getY(i),p.getY(i+1));//Y_min
        sides(i,1) = std::max(p.getY(i),p.getY(i+1));//Y_max
        if(sides(i,0) == p.getY(i))
            sides(i,2) = p.getX(i); //X of Y_min
        else
            sides(i,2) = p.getX(i+1); //X of Y_min
        sides(i,3) = 1.0 / ( ( (float)p.getY(i)-p.getY(i+1) ) / ( p.getX(i)-p.getX(i+1) ) ); // 1/m
    }
    for(int row = 0; row < buffer.getnLines(); row++){
        std::vector<int> xs;
        for(int i = 0; i < size; i++){
            if(row >= sides(i,0) && row < sides(i,1)){
                int x = round(sides(i,3)*(row-sides(i,0))+sides(i,2));
                xs.push_back(x);
            }
        }
        if(xs.size() <= 1){
            continue;
        }else{
            std::sort(xs.begin(),xs.end());
            int index = 0;
            bool draw = false;
            for(int col = xs[index]; col < buffer.getnColumns() && index < xs.size(); col++){
                if(col == xs[index]){
                    index++;
                    draw = !draw;
                    col--;
                }else if(draw){
                    drawPixel(col,row,color);
                }
            }
        }
    }
}

void Tech::addPolygon(Polygon &poly){
	Polygon poly_c(poly);
	polygons.push_back(poly_c);
}

void Tech::addPolygonAt(const int index, Polygon &poly){
    Polygon poly_c(poly);
    polygons.insert(polygons.begin()+index, poly_c);
}

void Tech::addPoint(const int index, const short int x, const short int y){
	if(polygons.size() > index)
		polygons[index].addPoint(x,y);
	else
		std::cout << "Almost a segmentation fault!" << std::endl;
}

void Tech::updateBuffer(){
	for(int i = 0; i < buffer.getnColumns(); i++)
		for(int j = 0; j < buffer.getnLines(); j++)
			drawPixel(i,j,-1);
	
	for(int i = 0; i < polygons.size(); i++){
		switch(polygons[i].getType()){
			case T_POINT:
				drawPixel(polygons[i].getX(0), polygons[i].getY(0), polygons[i].getColor());
				break;
			case T_LINE:
				drawLine(polygons[i].getX(0), polygons[i].getY(0), polygons[i].getX(1), polygons[i].getY(1), polygons[i].getColor());
				break;
			case T_POLYLINE:
				for(int j = 1; j < polygons[i].getNPoints(); j++)
					drawLine(polygons[i].getX(j-1), polygons[i].getY(j-1), polygons[i].getX(j), polygons[i].getY(j), polygons[i].getColor());
				break;
			case T_POLYGON:
                for(int j = 1; j < polygons[i].getNPoints(); j++)
                    if(polygons[i].getPoint(j).isVisible())
                        drawLine(polygons[i].getX(j-1), polygons[i].getY(j-1), polygons[i].getX(j), polygons[i].getY(j), polygons[i].getColor());
                if(polygons[i].isFilled()){
                    scanLine(polygons[i]);
				}
				break;
			default:
				std::cout << "\nerror! unkown polygon type!" << std::endl;
		}
	}
}

void Tech::clear(){
    polygons.clear();
    cutIndexes.clear();
    buffer.clear();
}

void Tech::setPolygon(const int index, const Polygon &poly){
	if(polygons.size() > index)
		polygons[index] = poly;
	else
		std::cout << "Almost a segmentation fault!" << std::endl;
}

void Tech::setPosition(const int polyId, const int pointId, const short int x, const short int y){
    if(polygons.size() > polyId)
        polygons[polyId].setPos(pointId, x, y, 0);
    else
        std::cout << "Almost a segmentation fault! Tech::setPolygon()" << std::endl;
}

void Tech::setPosition(const int polyId, const int pointId, const short int x, const short int y, const short int z){
    if(polygons.size() > polyId)
        polygons[polyId].setPos(pointId, x, y, z);
    else
        std::cout << "Almost a segmentation fault! Tech::setPolygon()" << std::endl;
}

void Tech::setVisibility(const short int polyId, const short int pointId, const bool visibility){
    polygons[polyId].setVisibility(pointId,visibility);
}


void Tech::cutRegion(const short int x1, const short int y1, const short int x2, short const int y2){
    short int botton = std::min(y1,y2);
    short int up     = std::max(y1,y2);
    short int left   = std::min(x1,x2);
    short int right  = std::max(x1,x2);
    cutIndexes.clear();

    cohenSutherland(botton, up, left, right);
    sutherlandHodgman(botton, up, left, right);
}

void Tech::cohenSutherland(const short int botton, const short int up, const short int left, short const int right){
    for(int i = 0; i < polygons.size(); i++){
        Polygon poly = polygons[i];
        if(polygons[i].getType() == T_POINT){
            int x = poly.getX(0);
            int y = poly.getY(0);
            if(botton < y && up > y && left < x && right > x){
                cutIndexes.push_back(i);
            }
        }else if(poly.getType() == T_LINE){
            short int x_i, y_i, diffBit;
            int code  = splitLine(poly, diffBit, x_i, y_i, botton, up, left, right);
            if(code == LINE_IN){
                cutIndexes.push_back(i);
            }else if(code == LINE_OUT){
                continue;
            }else if(code == LINE_CUT && !(x_i == poly.getX(0) && y_i == poly.getY(0)) && !(x_i == poly.getX(1) && y_i == poly.getY(1))){
                Polygon other = polygons[i];
                poly.setPos(0,x_i,y_i,0);
                other.setPos(1,x_i,y_i,0);
                setPolygon(i,poly);
                addPolygon(other);
                i--;
            }
        }else if(poly.getType() == T_POLYLINE || (poly.getType() == T_POLYGON && !poly.isFilled())){
            Polygon polyOrig(T_POLYLINE, poly.getColor());
            Polygon other(T_POLYLINE, poly.getColor());
            bool inside = true;
            for(int j = 0; j < poly.getNPoints()-1; j++){
                Polygon line(T_LINE);
                line.addPoint(poly.getPoint(j));
                line.addPoint(poly.getPoint(j+1));
                short int x_i, y_i, diffBit;
                int code  = splitLine(line, diffBit, x_i, y_i, botton, up, left, right);
                if(code == LINE_IN){
                    polyOrig.addPoint(poly.getPoint(j));
                    continue;
                }else if(code == LINE_OUT){
                    polyOrig.addPoint(poly.getPoint(j));
                    inside = false;
                }else if(code == LINE_CUT && !(x_i == line.getX(0) && y_i == line.getY(0)) && !(x_i == line.getX(1) && y_i == line.getY(1))){
                    polyOrig.addPoint(poly.getPoint(j));
                    polyOrig.addPoint(x_i,y_i,0);
                    other.addPoint(x_i,y_i,0);
                    for(int k = j+1; k < poly.getNPoints(); k++)
                        other.addPoint(poly.getPoint(k));
                    inside = false;
                    break;
                }else{
                    polyOrig.addPoint(poly.getPoint(j));
                    inside = false;
                }
            }
            if(other.getNPoints() == 0 && inside){
                cutIndexes.push_back(i);
            }else if(other.getNPoints() > 0){
                setPolygon(i,polyOrig);
                addPolygonAt(i+1,other);
                i--;
            }
        }
    }
}

void Tech::sutherlandHodgman(const short int botton, const short int up, const short int left, short const int right){
    Point p1(left,botton), p2(right,botton), p3(left,up), p4(right,up);

    for(int i = 0; i < polygons.size(); i++){
        Polygon poly = polygons[i];
        if(poly.getType() != T_POLYGON || !poly.isFilled())
            continue;
        Polygon polyOrig(T_POLYGON, poly.getColor(), true);
        Polygon other(T_POLYGON, poly.getColor(), true);
        bool inside = true;
        for(int j = 0; j < poly.getNPoints()-1; j++){
            Polygon line(T_LINE);
            line.addPoint(poly.getPoint(j));
            line.addPoint(poly.getPoint(j+1));
            short int x_i, y_i, diffBit;
            int code  = splitLine(line, diffBit, x_i, y_i, botton, up, left, right);
            Point p_i(x_i,y_i);

            if(code == LINE_IN){
                polyOrig.addPoint(poly.getPoint(j));
                continue;
            }else if(code == LINE_OUT){
                polyOrig.addPoint(poly.getPoint(j));
                inside = false;
            }else if(code == LINE_CUT && line.getPoint(0) != p_i && line.getPoint(1) != p_i){

                //The splitLine function works with a bounding box. The sutherland
                // algorithm works with lines. So we try to make a giant box
                // taking big number for the other ones.
                short int origDiff = diffBit;
                short int upB, bottonB, rightB, leftB;
                upB     = (diffBit == 3 ? up     :  1e4);
                bottonB = (diffBit == 2 ? botton : -1e4);
                rightB  = (diffBit == 1 ? right  :  1e4);
                leftB   = (diffBit == 0 ? left   : -1e4);

                polyOrig.clear();
                other.clear();
                bool otherAdd = false;
                for(int k = 0; k < poly.getNPoints()-1; k++){
                    Polygon lineB(T_LINE);
                    lineB.addPoint(poly.getPoint(k));
                    lineB.addPoint(poly.getPoint(k+1));
                    short int x_i, y_i, diffBit;
                    int code  = splitLine(lineB, diffBit, x_i, y_i, bottonB, upB, leftB, rightB);
                    if(code == LINE_CUT && !otherAdd){
                        polyOrig.addPoint(poly.getPoint(k));
                        polyOrig.addPoint(x_i,y_i,0);
                        other.addPoint(x_i,y_i,0);
                        otherAdd = true;
                    }else if(code == LINE_CUT && otherAdd){
                        other.addPoint(poly.getPoint(k));
                        other.addPoint(x_i,y_i,0);
                        other.addPoint(other.getPoint(0));
                        polyOrig.addPoint(x_i,y_i);
                        otherAdd = false;
                    }else if(otherAdd){
                        other.addPoint(poly.getPoint(k));
                    }else {
                        polyOrig.addPoint(poly.getPoint(k));
                    }
                }

                polyOrig.addPoint(poly.getPoint(poly.getNPoints()-1));
                inside = false;
                break;
            }else{
                polyOrig.addPoint(poly.getPoint(j));
                inside = false;
            }
        }
        if(other.getNPoints() == 0 && inside){
            cutIndexes.push_back(i);
        }else if(other.getNPoints() > 2){
            setPolygon(i,polyOrig);
            addPolygonAt(i+1,other);
            i--;
        }
    }
}

void Tech::translate(const int dx, const int dy, const int dz, const bool onlyCut){
    std::vector<int> ind;
    if(onlyCut){
        ind.resize(cutIndexes.size());
        for(int i = 0; i < cutIndexes.size(); i++)
            ind[i] = cutIndexes[i];
    }else{
        ind.resize(polygons.size());
        for(int i = 0; i < polygons.size(); i++)
            ind[i] = i;
    }

    Matrix transMat(4,4);
    transMat(0,0) = 1; transMat(0,1) = 0; transMat(0,2) = 0; transMat(0,3) = dx;
    transMat(1,0) = 0; transMat(1,1) = 1; transMat(1,2) = 0; transMat(1,3) = dy;
    transMat(2,0) = 0; transMat(2,1) = 0; transMat(2,2) = 1; transMat(2,3) = dz;
    transMat(3,0) = 0; transMat(3,1) = 0; transMat(3,2) = 0; transMat(3,3) =  1;

    for(int i = 0; i < ind.size(); i++){
        Polygon poly = polygons[ind[i]];
        int sizePoly = poly.getNPoints();
        Matrix coord(4,sizePoly);
        for(int j = 0; j < sizePoly; j++){
            coord(0,j) = poly.getX(j);
            coord(1,j) = poly.getY(j);
            coord(2,j) = poly.getZ(j);
            coord(3,j) =            1;
        }

        Matrix res(4,sizePoly);
        res = transMat * coord;

        for(int j = 0; j < sizePoly; j++){
            poly.setPos(j,res(0,j),res(1,j),res(2,j));
        }
        polygons[ind[i]] = poly;
    }
}

void Tech::rotate(const int degree, const bool onlyCut){
    double angle =  degree * PI / 180.0;

    std::vector<int> ind;
    if(onlyCut){
        ind.resize(cutIndexes.size());
        for(int i = 0; i < cutIndexes.size(); i++)
            ind[i] = cutIndexes[i];
    }else{
        ind.resize(polygons.size());
        for(int i = 0; i < polygons.size(); i++)
            ind[i] = i;
    }

    Matrix transMat(3,3);
    transMat(0,0) = cos(angle); transMat(0,1) = -sin(angle); transMat(0,2) = 0;
    transMat(1,0) = sin(angle); transMat(1,1) =  cos(angle); transMat(1,2) = 0;
    transMat(2,0) =          0; transMat(2,1) =           0; transMat(2,2) = 1;

    int meanX = buffer.getnColumns() / 2,
        meanY = buffer.getnLines()   / 2;

    for(int i = 0; i < ind.size(); i++){
        Polygon poly = polygons[ind[i]];
        int sizePoly = poly.getNPoints();
        Matrix coord(3,sizePoly);
        for(int j = 0; j < sizePoly; j++){
            coord(0,j) = poly.getX(j) - meanX;
            coord(1,j) = poly.getY(j) - meanY;
            coord(2,j) =            1;
        }

        Matrix res(3,sizePoly);
        res = transMat * coord;

        for(int j = 0; j < sizePoly; j++){
            poly.setPos(j,res(0,j) + meanX,res(1,j) + meanY,0);
        }
        polygons[ind[i]] = poly;
    }
}

void Tech::scale(const float dx, const float dy, const float dz, const bool onlyCut){

    std::vector<int> ind;
    if(onlyCut){
        ind.resize(cutIndexes.size());
        for(int i = 0; i < cutIndexes.size(); i++)
            ind[i] = cutIndexes[i];
    }else{
        ind.resize(polygons.size());
        for(int i = 0; i < polygons.size(); i++)
            ind[i] = i;
    }

    Matrix transMat(4,4);
    transMat(0,0) = dx; transMat(0,1) =  0; transMat(0,2) =  0; transMat(0,3) = 0;
    transMat(1,0) =  0; transMat(1,1) = dy; transMat(1,2) =  0; transMat(1,3) = 0;
    transMat(2,0) =  0; transMat(2,1) =  0; transMat(2,2) = dz; transMat(2,3) = 0;
    transMat(3,0) =  0; transMat(3,1) =  0; transMat(3,2) =  0; transMat(3,3) = 1;

    int meanX = buffer.getnColumns() / 2,
        meanY = buffer.getnLines()   / 2,
        meanZ = 0; /**/

    for(int i = 0; i < ind.size(); i++){
        Polygon poly = polygons[ind[i]];
        int sizePoly = poly.getNPoints();
        Matrix coord(4,sizePoly);
        for(int j = 0; j < sizePoly; j++){
            coord(0,j) = poly.getX(j) - meanX;
            coord(1,j) = poly.getY(j) - meanY;
            coord(2,j) = poly.getZ(j) - meanZ;
            coord(3,j) =            1;
        }

        Matrix res(4,sizePoly);
        res = transMat * coord;

        for(int j = 0; j < sizePoly; j++){
            poly.setPos(j,res(0,j)+meanX,res(1,j)+meanY,0+meanZ);
        }
        polygons[ind[i]] = poly;
    }
}

int Tech::splitLine(Polygon poly, short int &diffBit, short int &x_i, short int &y_i, const short int botton, const short int up, const short int left, short const int right){
    if(poly.getType() != T_LINE){
        std::cout << "splitLine: The polygon is not a line!" << std::endl;
        return INVALID_LINE;
    }

    int x1 = poly.getX(0),
        x2 = poly.getX(1),
        y1 = poly.getY(0),
        y2 = poly.getY(1);

    std::bitset<4> c1(0);
    std::bitset<4> c2(0);
    c1[3] = (up - y1    ) < 0;
    c1[2] = (y1 - botton) < 0;
    c1[1] = (right - x1 ) < 0;
    c1[0] = (x1 - left  ) < 0;
    c2[3] = (up - y2    ) < 0;
    c2[2] = (y2 - botton) < 0;
    c2[1] = (right - x2 ) < 0;
    c2[0] = (x2 - left  ) < 0;

    bool found = false;
    if((c1 | c2) == 0000)
        return LINE_IN;
    else if((c1 & c2) != 0000)
        return LINE_OUT;
    else{
        float m = ((float)y2-y1)/(x2-x1);

        if(c1[3] != c2[3] && m != 0){
            y_i = up;
            x_i = floor((y_i-y1)/m+x1+0.5);
            if(x_i >= left && x_i <= right && y_i >= botton && y_i <= up){
                found = true;
                diffBit = 3;
            }
        }
        if(c1[2] != c2[2] && m != 0 && !found){
            y_i = botton;
            x_i = ceil((y_i-y1)/m+x1-0.5);
            if(x_i >= left && x_i <= right && y_i >= botton && y_i <= up){
                found = true;
                diffBit = 2;
            }
        }
        if(c1[1] != c2[1] && !found){
            x_i = right;
            y_i = floor((x_i-x1)*m+y1+0.5);
            if(x_i >= left && x_i <= right && y_i >= botton && y_i <= up){
                found = true;
                diffBit = 1;
            }
        }
        if(c1[0] != c2[0] && !found){
            x_i = left;
            y_i = ceil((x_i-x1)*m+y1-0.5);
            if(x_i >= left && x_i <= right && y_i >= botton && y_i <= up){
                found = true;
                diffBit = 0;
            }
        }
    }
    if(found)
        return LINE_CUT;
    else{
        return LINE_OUT;
        diffBit = -1;
    }
}

Polygon Tech::getPolygon(const short int index){
	if(polygons.size() > index)
		return polygons[index];
	else
		std::cout << "Almost a segmentation fault!" << std::endl;
}

int Tech::getCutIndex(const int index){
    if(index >= 0 && index < cutIndexes.size())
        return cutIndexes[index];
    else{
        std::cout << "almost segmentation fault! getCutIndex" << std::endl;
        return -1;
    }
}

int Tech::getNCutPoly(){
    return cutIndexes.size();
}

int Tech::getNPolygons(){
	return polygons.size();
}
