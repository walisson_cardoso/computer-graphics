
#include "point.h"

Point::Point() {
    visible = true;
	position.resize(3);
	position[0] = 0;
	position[1] = 0;
	position[2] = 0;
}

Point::Point(const short int x) {
    visible = true;
	position.resize(3);
	position[0] = x;
	position[1] = 0;
	position[2] = 0;
};
	
Point::Point(const short int x, const short int y) {
    visible = true;
	position.resize(3);
	position[0] = x;
	position[1] = y;
	position[2] = 0;
}

Point::Point(const short int x, const short int y, const short int z) {
    visible = true;
	position.resize(3);
	position[0] = x;
	position[1] = y;
	position[2] = z;
}

Point::Point(const Point &other){
    this->visible = other.visible;
	this->position.resize(3);
	this->position[0] = other.position[0];
	this->position[1] = other.position[1];
	this->position[2] = other.position[2];
}

void Point::setPos(const short int x, const short int y, const short int z){
	position[0] = x;
	position[1] = y;
	position[2] = z;
}

short int Point::getX(){
	return position[0];
}

short int Point::getY(){
	return position[1];
}

short int Point::getZ(){
	return position[2];
}

bool Point::isVisible(){
    return visible;
}

void Point::setVisibility(const bool visible){
    this->visible = visible;
}

Point& Point::operator=( const Point& other ) {
    this->visible = other.visible;
	this->position.resize(3);
	this->position[0] = other.position[0];
	this->position[1] = other.position[1];
	this->position[2] = other.position[2];
	return *this;
}

bool Point::operator==(const Point& other){
    if(this->position[0] == other.position[0] && this->position[1] == other.position[1]
            && this->position[2] == other.position[2] && this->visible == other.visible)
        return true;
    else
        return false;
}

bool Point::operator!=(const Point& other){
    return !(*this == other);
}

std::ostream& operator<<( std::ostream &os, Point& other ){
    os << "\n" << "X = " << other.getX() << " Y = " << other.getY() << " Z = " << other.getZ();
    return os;
}

