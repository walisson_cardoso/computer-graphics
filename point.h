
#include <iostream>
#include <vector>

class Point {
public:
	Point();
	Point(const short int x);
	Point(const short int x, const short int y);
	Point(const short int x, const short int y, const short int z);
	Point(const Point &other);
	void setPos(const short int x, const short int y, const short int z);
	short int getX();
	short int getY();
	short int getZ();
    bool isVisible();
    void setVisibility(const bool visible);
    Point& operator=(const Point& other);
    bool operator==(const Point& other);
    bool operator!=(const Point& other);
    friend std::ostream& operator<<( std::ostream &os, Point& other );
private:
	std::vector<short int> position;
    bool visible;
};
