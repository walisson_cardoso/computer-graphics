
#include <GL/glut.h>
#include "tech.h"

#define WIDTH 640
#define TWIDTH 740
#define HEIGHT 480

int twidth = TWIDTH;
int width = WIDTH;
int height = HEIGHT;

float sep_prop = 0.16;

short int nLines = 192;
short int nColumns = 256;
bool showGrid = false;
bool key_state[256] = { false };
bool mouse_button_down = false;

short int color = BLACK;
short int colorb = RED;
short int last_color = color;
short int tool = PENCIL;
bool polyFollow = false;
bool cutLock = false;
int choose_poly = 0;
int choose_vertex = 0;

Tech tech;


void grid() {
	
	glColor3f(0.0,0.0,0.0);
	glLineWidth(1.0f);
	glBegin(GL_LINES);
	
	for(int i = 0; i <= nColumns; i++){
		glVertex2f(i, 0.0);
		glVertex2f(i, nLines);
	}
	
	for(int i = 0; i <= nLines; i++){
		glVertex2f(0.0, i);
		glVertex2f(nColumns, i);
	}
        
	glEnd();
}

void displayTools(){
	
	glColor3f(0.0,0.0,0.0);
	float x_s = nColumns + 1,
		  x_e = (1+sep_prop)*nColumns - 1,
		  y_s = 0.5,
		  y_e = nLines - 0.5,
		  x_d = x_e - x_s,
		  y_d = y_e - y_s,
		  x_m = x_d/10, //Regula valores mágicos
		  y_m = y_d/50; //Regula valores mágicos
		  
	float pass = (y_e - 0.5 * y_e) / 5;
	
	std::bitset<3> b(BLACK);
		
	glBegin(GL_LINES);
		
		//Draws tool's box
		glVertex2f(x_s,y_e);
		glVertex2f(x_s,0.5 * y_e);
		
		glVertex2f(x_e,y_e);
		glVertex2f(x_e,0.5 * y_e);
		
		
		for(float y = y_e; y >= 0.5 * y_e - 1e-5; y -= pass){
			glVertex2f(x_s, y);
			glVertex2f(x_e, y);
		}
		
		glVertex2f((x_e-x_s)/2+x_s,y_e);
		glVertex2f((x_e-x_s)/2+x_s,y_e/2);
		
		//Draws tools
		//Pencil
		b = (tool == PENCIL) ? RED : BLACK;
		glColor3f(b[0], b[1], b[2]);
			
		glVertex2f(x_d/4+x_s-0.5*x_m,y_e-pass/2);
		glVertex2f(x_d/4+x_s+0.5*x_m,y_e-pass/2);
		
		glVertex2f(x_d/4+x_s,y_e-pass/2-0.5*y_m);
		glVertex2f(x_d/4+x_s,y_e-pass/2+0.5*y_m);
		
		//Line
		b = (tool == LINE) ? RED : BLACK;
		glColor3f(b[0], b[1], b[2]);
		
		glVertex2f(x_e-x_d/4+1.3*x_m,y_e-pass/2+1.3*y_m);
		glVertex2f(x_e-x_d/4-1.3*x_m,y_e-pass/2-1.3*y_m);
		
		//Polyline
		b = (tool == POLYLINE) ? RED : BLACK;
		glColor3f(b[0], b[1], b[2]);
		
		glVertex2f(x_s+1*x_m,y_e-pass-0.8*y_m);
		glVertex2f(x_s+3.5*x_m,y_e-pass-0.5*y_m);
		
		glVertex2f(x_s+3.5*x_m,y_e-pass-0.5*y_m);
		glVertex2f(x_s+3.0*x_m,y_e-pass-3.5*y_m);
		
		glVertex2f(x_s+3.0*x_m,y_e-pass-3.5*y_m);
		glVertex2f(x_s+1.7*x_m,y_e-pass-4.1*y_m);
		
		//Polygon
		b = (tool == POLYGON) ? RED : BLACK;
		glColor3f(b[0], b[1], b[2]);
		
		glVertex2f(x_e-x_d/4,y_e-pass-0.8*y_m);
		glVertex2f(x_e-x_d/4+1.5*x_m,y_e-pass-4.0*y_m);
		
		glVertex2f(x_e-x_d/4+1.5*x_m,y_e-pass-4.0*y_m);
		glVertex2f(x_e-x_d/4-1.5*x_m,y_e-pass-4.0*y_m);

		glVertex2f(x_e-x_d/4-1.5*x_m,y_e-pass-4.0*y_m);
		glVertex2f(x_e-x_d/4-2.0*x_m,y_e-pass-3.2*y_m);
		
		glVertex2f(x_e-x_d/4-2.0*x_m,y_e-pass-3.2*y_m);
		glVertex2f(x_e-x_d/4,y_e-pass-0.8*y_m);
		
		//Rectangle
		b = (tool == RECTANGLE) ? RED : BLACK;
		glColor3f(b[0], b[1], b[2]);
		
		glVertex2f(x_s+1*x_m,y_e-2*pass-1.3*y_m);
		glVertex2f(x_s+3*x_m,y_e-2*pass-1.3*y_m);
		
		glVertex2f(x_s+3*x_m,y_e-2*pass-1.3*y_m);
		glVertex2f(x_s+3*x_m,y_e-2*pass-3.3*y_m);
		
		glVertex2f(x_s+3*x_m,y_e-2*pass-3.3*y_m);
		glVertex2f(x_s+1*x_m,y_e-2*pass-3.3*y_m);
		
		glVertex2f(x_s+1*x_m,y_e-2*pass-3.3*y_m);
		glVertex2f(x_s+1*x_m,y_e-2*pass-1.3*y_m);
		
	glEnd();
	
	//Fill
	b = (tool == FILL) ? RED : BLACK;
	glColor3f(b[0], b[1], b[2]);
		
	glRectf(x_s+1*x_m+x_d/2,y_e-2*pass-1.3*y_m, x_s+3*x_m+x_d/2,y_e-2*pass-3.3*y_m);
	glRectf(x_s+1.6*x_m+x_d/2,y_e-2*pass-1.9*y_m, x_s+3.6*x_m+x_d/2,y_e-2*pass-3.9*y_m);
	
	//cut
	b = (tool == CUT) ? RED : BLACK;
	glColor3f(b[0], b[1], b[2]);
	
	glRectf(x_s+1*x_m,y_e-3*pass-1.3*y_m, x_s+1.7*x_m,y_e-3*pass-3.3*y_m);
	glRectf(x_s+1.9*x_m,y_e-3*pass-1.3*y_m, x_s+3*x_m,y_e-3*pass-3.3*y_m);
	
	//Colors Palette
	for(int i = 0; i < 2; i++)
		for(int j = 0; j < 4; j++){
			std::bitset<3> b(i*4+j);
			glColor3f(b[0], b[1], b[2]);
			glRectf(x_s+j*x_d/4,y_e-(i*0.5+5.5)*pass, x_s+(j+1)*x_d/4,y_e-((i+1)*0.5+5.5)*pass);
		}
	
	//Selected Colors
	b = color;
	glColor3f(b[0], b[1], b[2]);
	glRectf(x_s,y_e-7*pass, x_s+x_d/2,y_e-8*pass);
	b = colorb;
	glColor3f(b[0], b[1], b[2]);
	glRectf(x_s+x_d/2,y_e-7*pass, x_e,y_e-8*pass);
	
	b = color;
	glColor3f(b[0], b[1], b[2]);
}

void toolClick(float x, float y, int button){
	float x_s = nColumns + 0.5,
	  x_e = (1+sep_prop)*nColumns - 1.5,
	  y_s = 0.5,
	  y_e = nLines - 1,
	  x_d = x_e - x_s,
	  y_d = y_e - y_s;
	  
	float pass = (y_e - 0.5 * y_e) / 5;
	  
	if(y < y_e && y > y_e/2){
		if(x > x_s && x < x_s+x_d/2){
			if(y <= y_e-0*pass && y > y_e-1*pass){
				tool = PENCIL;
			}if(y <= y_e-1*pass && y > y_e-2*pass){
				tool = POLYLINE;
				polyFollow = false;
			}if(y <= y_e-2*pass && y > y_e-3*pass){
				tool = RECTANGLE;
				polyFollow = false;
			}if(y <= y_e-3*pass && y > y_e-4*pass){
				tool = CUT;
				polyFollow = false;
                cutLock = false;
			}if(y <= y_e-4*pass && y > y_e-5*pass){
				std::cout << "\nNo tool here!";
				polyFollow = false;
			}
		}else if(x >= x_s+x_d/2 && x < x_e){
			if(y <= y_e-0*pass && y > y_e-1*pass){
				tool = LINE;
				polyFollow = false;
			}if(y <= y_e-1*pass && y > y_e-2*pass){
				tool = POLYGON;
				polyFollow = false;
			}if(y <= y_e-2*pass && y > y_e-3*pass){
				tool = FILL;
				polyFollow = false;
			}if(y <= y_e-3*pass && y > y_e-4*pass){
				std::cout << "\nNo tool here!" << std::endl;
				polyFollow = false;
			}if(y <= y_e-4*pass && y > y_e-5*pass){
				std::cout << "\nNo tool here!" << std::endl;
				polyFollow = false;
			}	
		}
	}else if(y >= y_e/2-1.5*pass && y < y_e/2-0.5*pass){ //color palette
		int y_c = (y > y_e/2-1.0*pass) ? 0 : 1;
		int x_c = floor((x - x_s)/(x_d/4));
		if(button == GLUT_LEFT_BUTTON)
			color = y_c*4+x_c;
		else
			colorb = y_c*4+x_c;
	}else{
		color=2;
	}
}

void updateScreen() {

	glClear(GL_COLOR_BUFFER_BIT);

    displayTools();
	
	if(twidth != glutGet(GLUT_WINDOW_WIDTH)){
		twidth = glutGet(GLUT_WINDOW_WIDTH);
		width = (nColumns+1)*twidth/((1+sep_prop)*nColumns+1);
	}
	height = glutGet(GLUT_WINDOW_HEIGHT);
		
    for(int i = 0; i < nColumns; i++)
		for(int j = 0; j < nLines; j++)
			if(tech.getPixel(i,j) >= 0){
				std::bitset<3> b(tech.getPixel(i,j));
				glColor3f(b[0], b[1], b[2]);
				glRectf(i, j+1, i+1, j);
			}

	if(showGrid)
        grid();

	glutSwapBuffers();
}

void keySpecials(){
    bool cutOnly = (tool == CUT && cutLock);
    if(key_state['e'] == true){
        tech.clear();
        color = BLACK;
        colorb = RED;
        last_color = color;
        tool = PENCIL;
        polyFollow = false;
        cutLock = false;
        choose_poly = 0;
        choose_vertex = 0;
        updateScreen();
    }else if(key_state['w'] == true){
        tech.translate(0,10,0,cutOnly);
    }else if(key_state['a'] == true){
        tech.translate(-10,0,0,cutOnly);
    }else if(key_state['d'] == true){
        tech.translate(10,0,0,cutOnly);
    }else if(key_state['s'] == true){
        tech.translate(0,-10,0,cutOnly);
    }else if(key_state['r'] == true){
        tech.rotate(10, cutOnly);
    }else if(key_state['t'] == true){
        tech.rotate(-10, cutOnly);
    }else if(key_state['x'] == true){
        tech.scale(1.1,1,1,cutOnly);
    }else if(key_state['c'] == true){
        tech.scale(0.9,1,1,cutOnly);
    }else if(key_state['y'] == true){
        tech.scale(1,1.1,1,cutOnly);
    }else if(key_state['u'] == true){
        tech.scale(1,0.9,1,cutOnly);
    }

    tech.updateBuffer();
    updateScreen();
}

void key_down(unsigned char key, int x, int y){
    key_state[key] = true;
    keySpecials();
}

void key_up(unsigned char key, int x, int y){
    key_state[key] = false;
}

void mouseMotion(int x, int y){

    short int g_x =          round(x * (nColumns+1) / float(width)) - 1;
    short int g_y = nLines - round(y * (nLines+1) / float(height));

    if(key_state['z'] == true){
        if(mouse_button_down && choose_poly != -1 && choose_vertex != -1){
            int size = tech.getPolygon(choose_poly).getNPoints();
            if(size > 2 && (size == choose_vertex-1 || choose_vertex == 0) && tech.getPolygon(choose_poly).getType() == T_POLYGON){
                tech.setPosition(choose_poly, 0, g_x, g_y);
                tech.setPosition(choose_poly, size-1, g_x, g_y);
            }else
                tech.setPosition(choose_poly, choose_vertex, g_x, g_y);
        }
    }else if(tool == PENCIL){
        static short int last_x = -1, last_y = -1;
        if(last_x != g_x || last_y != g_y){
            last_x = g_x; last_y = g_y;
            Polygon poly(T_POINT, last_color);
            poly.addPoint(g_x, g_y);
            tech.addPolygon(poly);
        }
    }else if(tool == CUT && cutLock){
        static short int last_x = g_x, last_y = g_y;
        if(abs(last_x - g_x) > nLines/10+1 || abs(last_y - g_y) > nLines/10+1){
            last_x = g_x;
            last_y = g_y;
        }
        if(last_x != g_x || last_y != g_y){
            int nPoly = tech.getNCutPoly();
            for(int i = 0; i < nPoly; i++){
                int index = tech.getCutIndex(i);
                Polygon poly = tech.getPolygon(index);
                int sizePoly = poly.getNPoints();
                Matrix coord(sizePoly,2);
                for(int j = 0; j < sizePoly; j++){
                    short int dx = poly.getX(j),
                              dy = poly.getY(j);
                    dx += g_x-last_x;
                    dy += g_y-last_y;
                    poly.setPos(j,dx,dy,0);
                }
                tech.setPolygon(index,poly);
            }
            last_x = g_x; last_y = g_y;
        }
    }
    tech.updateBuffer();
    updateScreen();
}

void manageMouse(int button, int state, int x, int y){
    if (button == GLUT_LEFT_BUTTON || button == GLUT_RIGHT_BUTTON){
		int sec_color = (button == GLUT_LEFT_BUTTON ? color : colorb);
        last_color = sec_color;

        if(state == GLUT_DOWN)
            mouse_button_down = true;
        else
            mouse_button_down = false;

		short int g_x =          round(x * (nColumns+1) / float(width)) - 1;
		short int g_y = nLines - round(y * (nLines+1) / float(height));
		
        if(key_state['z'] == true){
            int closest = 1e5;
            for(int i = 0; i < tech.getNPolygons(); i++){
                Polygon poly_aux(tech.getPolygon(i));
                for(int j = 0; j < poly_aux.getNPoints(); j++){
                    int dist = pow(poly_aux.getX(j)-g_x, 2) + pow(poly_aux.getY(j)-g_y, 2);
                    if(dist < closest){
                        choose_poly = i;
                        choose_vertex = j;
                        closest = dist;
                    }
                }
            }
            if(closest > nLines*nLines/200+1){
                choose_poly = -1;
                choose_vertex = -1;
            }
        }else if(x < width && state == GLUT_DOWN){
            switch(tool){
				case PENCIL: {
					Polygon poly(T_POINT, sec_color);
					poly.addPoint(g_x, g_y);
					tech.addPolygon(poly);
					tech.updateBuffer();
					break;
				}case LINE: {
					static short int last_x = -1, last_y = -1;
					if(!polyFollow){
						last_x = g_x; last_y = g_y;
						polyFollow = true;
					}else{
						polyFollow = false;
						Polygon poly(T_LINE, sec_color);
								poly.addPoint(last_x, last_y); last_x = -1; last_y = -1;
								poly.addPoint(g_x, g_y);
						tech.addPolygon(poly);
						tech.updateBuffer();
					}
					break;
				}case POLYLINE: {
					if(!polyFollow){
						Polygon poly(T_POLYLINE, sec_color);
								poly.addPoint(g_x, g_y);
						tech.addPolygon(poly);
						polyFollow = true;
					}else{
						int index = tech.getNPolygons()-1;
						tech.addPoint(index,g_x,g_y);
						tech.updateBuffer();
					}
					break;
				}case POLYGON: {
					if(!polyFollow){
						Polygon poly(T_POLYGON, sec_color);
								poly.addPoint(g_x, g_y);
								poly.addPoint(g_x, g_y);
						tech.addPolygon(poly);
						polyFollow = true;
					}else{
						int index = tech.getNPolygons()-1;
						Polygon poly = tech.getPolygon(index);
						int index2 = poly.getNPoints()-1;
						Point init = poly.getPoint(0);
						poly.setPos(index2, g_x, g_y, 0);
						poly.addPoint(init);
						tech.setPolygon(index,poly);
						tech.updateBuffer();
					}
					break;
				}case RECTANGLE: {
					static short int last_x = -1, last_y = -1;
					if(!polyFollow){
						last_x = g_x; last_y = g_y;
						polyFollow = true;
					}else{
						polyFollow = false;
						Polygon poly(T_POLYGON, sec_color);
								poly.addPoint(last_x, last_y);
								poly.addPoint(   g_x, last_y);
								poly.addPoint(   g_x,    g_y);
								poly.addPoint(last_x,    g_y);
								poly.addPoint(last_x, last_y); last_x = -1; last_y = -1;
						tech.addPolygon(poly);
						tech.updateBuffer();
					}
					break;
				}case FILL: {
					if(!polyFollow){
						Polygon poly(T_POLYGON, sec_color, true);
								poly.addPoint(g_x, g_y);
								poly.addPoint(g_x, g_y);
						tech.addPolygon(poly);
						polyFollow = true;
					}else{
						int index = tech.getNPolygons()-1;
						Polygon poly = tech.getPolygon(index);
						int index2 = poly.getNPoints()-1;
						Point init = poly.getPoint(0);
						poly.setPos(index2, g_x, g_y, 0);
						poly.addPoint(init);
						tech.setPolygon(index,poly);
						tech.updateBuffer();
					}
					break;
                }case CUT: {
                    if(cutLock)
                        break;
                    static short int last_x = -1, last_y = -1;
                    if(!polyFollow){
                        last_x = g_x; last_y = g_y;
                        polyFollow = true;
                    }else{
                        polyFollow = false;
                        tech.cutRegion(last_x, last_y, g_x, g_y);
                        last_x = -1; last_y = -1;
                        tech.updateBuffer();
                    }
                    break;
                }default:
                    std::cout << "Tool not recognized!" << std::endl;
            }
        }else if(x < width && state == GLUT_UP){
            switch(tool){
                case PENCIL: {
                    break;
                }case LINE: {
                    break;
                }case POLYLINE: {
                    break;
                }case POLYGON: {
                    break;
                }case RECTANGLE: {
                    break;
                }case FILL: {
                    break;
                }case CUT: {
                    if(!polyFollow)
                        cutLock = true;
                    break;
                }default:
                    std::cout << "Tool not recognized!" << std::endl;
            }
        }else if(state == GLUT_DOWN){
			toolClick(g_x, g_y, button);
		}
	}
    glutPostRedisplay();
}

void initialize () {
	
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-0.5,nColumns+sep_prop*nColumns+0.5,-0.5,nLines+0.5);
	glMatrixMode(GL_MODELVIEW);
	
	displayTools();
}



int main(int argc, char** argv) {
		
	if(argc == 2){
		nLines = atoi(argv[1]);
	}else if(argc == 3){
		nLines = atoi(argv[1]);
		nColumns = atoi(argv[2]);
	}else if(argc == 4){
		nLines = atoi(argv[1]);
		nColumns = atoi(argv[2]);
		showGrid = (atoi(argv[3]) == 0 ? false : true);
	}
	
	tech.setBufferDimentions(nLines, nColumns);
	
	glutInit(&argc,argv);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	glutInitWindowSize(TWIDTH,HEIGHT);

	glutInitWindowPosition(100,100);

	glutCreateWindow("Vamos desenhar um pouco!");

	glutDisplayFunc(updateScreen);

    glutKeyboardFunc(key_down);

    glutKeyboardUpFunc(key_up);
	
	glutMouseFunc(manageMouse);

    glutMotionFunc(mouseMotion);

	initialize();

	glutMainLoop();
} 
