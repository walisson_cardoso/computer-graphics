
#include "polygon.h"

Polygon::Polygon(const short int type){
	this->type = type;
	this->color = 0;
	this->filled = false;
}

Polygon::Polygon(const short int type, const short int color){
	this->type = type;
	this->color = color;
	this->filled = false;
}

Polygon::Polygon(const short int type, const short int color, const bool filled){
	this->type = type;
	this->color = color;
	this->filled = filled;
}

Polygon::Polygon( const Polygon &other ){
	this->type = other.type;
	this->color = other.color;
	this->filled = other.filled;
	this->points.resize(other.points.size());
	for(int i = 0; i < points.size(); i++)
		points[i] = other.points[i];
}

void Polygon::addPoint(const Point &p){
    if(type == T_POINT && points.size() >= 1){
        std::cout << "Polygon::addPoint. Can't add more points to a point type" << std::endl;
    }else if(type == T_LINE && points.size() >= 2){
        std::cout << "Polygon::addPoint. Can't add more points to a line type" << std::endl;
    }else{
        points.push_back(p);
    }
}

void Polygon::addPoint(const short int x, const short int y){
	Point p(x,y);
	points.push_back(p);
}

void Polygon::addPoint(const short int x, const short int y, const short z){
	Point p(x,y,z);
	points.push_back(p);
}

void Polygon::setPos(const short int index, const short int x, const short int y, const short int z){
	if(points.size() > index)
		points[index].setPos(x,y,z);
    else if(points.size() == index)
        addPoint(x, y, z);
	else
		std::cout << "Almost a segmentation fault!" << std::endl;
}

void Polygon::setVisibility(const short int index, const bool visibility){
    points[index].setVisibility(visibility);
}

void Polygon::clear(){
    points.clear();
}

short int Polygon::getX(const short int index){
	if(points.size() > index)
		return points[index].getX();
	else
		std::cout << "Almost a segmentation fault!" << std::endl;
	return 0;
}

short int Polygon::getY(const short int index){
	if(points.size() > index)
		return points[index].getY();
	else
		std::cout << "Almost a segmentation fault!" << std::endl;
	return 0;
}

short int Polygon::getZ(const short int index){
	if(points.size() > index)
		return points[index].getZ();
	else
		std::cout << "Almost a segmentation fault!" << std::endl;
	return 0;
}

short int Polygon::getColor(){
	return this->color;
}

short int Polygon::getNPoints(){
	return points.size();
}

short int Polygon::getType(){
	return type;
}

Point Polygon::getPoint(const short int index){
	if(points.size() > index)
		return points[index];
	else
		std::cout << "Almost a segmentation fault!" << std::endl;
	return 0;
}

bool Polygon::isFilled(){
	return filled;
}

Polygon& Polygon::operator=( const Polygon& other ){
	this->type = other.type;
	this->color = other.color;
	this->filled = other.filled;
	this->points.resize(other.points.size());
	for(int i = 0; i < points.size(); i++)
		points[i] = other.points[i];
		
	return *this;
}
